class Story {
  String storyTitle;
  String choice1;
  String choice2;

  // Old way:
  // Story(String storyTitle, String choice1, String choice2) {
  //   this.storyTitle = storyTitle;
  //   this.choice1 = choice1;
  //   this.choice2 = choice2;
  // }

  // New way:
  Story({this.storyTitle, this.choice1, this.choice2});
  // Flutter infers the type and handles the assignments. {}, allows for named parameters and makes them optional.
}
